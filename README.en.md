# Vue clock component

### External dependence
This component uses 'dayjs' to achieve time acquisition and processing.

```
	npm install dayjs -s
```

### How to use it
Drop it directly into the components folder and use it in the application that you need to use
